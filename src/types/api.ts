/* eslint-disable import/prefer-default-export */
export const SUMMARY_API_URL = 'https://api.kawalcovid19.id/v1/api/case/summary';

export interface Summary {
  confirmed: number;
  activeCare: number;
  recovered: number;
  deceased: number;
}

export interface SummaryApiResponse extends Summary {
  metadata: {
    lastUpdatedAt: string;
  };
}

export interface PublicationItem {
  date: string; // Tanggal (format: yyyy-mm-dd)
  url?: string; // Link
  publication?: string; // Nama publikasi/media/webinar/podcast
  subject?: string; // Tema
  speaker?: string; // Narasumber/pembicara
  description?: string; // Pesan kunci
}

export interface PublicationMap {
  subtitle: string;
  children: PublicationItem[];
}
