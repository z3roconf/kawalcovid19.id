import { DEKONTAMINASI_HOAXES_API_URL } from 'types/dekontaminasi';
import fetch from './fetch';

export default async function mafindo<TResponse = any>(init?: RequestInit): Promise<TResponse> {
  const data = await fetch<TResponse>(`${DEKONTAMINASI_HOAXES_API_URL}`, init);
  return data;
}
