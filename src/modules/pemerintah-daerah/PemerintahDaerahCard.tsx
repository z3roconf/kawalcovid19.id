import * as React from 'react';

import styled from '@emotion/styled';

import { Box, Heading, Text, themeProps } from 'components/design-system';
import { OpenLinkIcon } from 'components/icons';
import { logEventClick } from 'utils/analytics';
import PemerintahDaerahLink from './PemerintahDaerahLink';

export interface PemerintahDaerahCardProps {
  name: string;
  link: string;
}

const BaseBox = styled(Box)`
  padding: ${themeProps.space.sm}px 0;
  ${themeProps.mediaQueries.md} {
    padding: ${themeProps.space.md}px 0;
  }
`;

const PemerintahDaerahCard: React.FC<PemerintahDaerahCardProps> = ({ name, link }) => {
  return (
    <PemerintahDaerahLink
      href={link}
      target="_blank"
      rel="noopener noreferrer"
      onClick={() => logEventClick(`[Pemda] ${name}`)}
    >
      <BaseBox
        display="flex"
        height="100%"
        alignItems="flex-start"
        justifyContent="space-between"
        borderBottom="1px solid"
        borderBottomColor="accents02"
      >
        <Box>
          <Heading variant={400} fontWeight={600} as="h5" mb="xxs">
            {name}
          </Heading>
          <Text variant={100} as="p">
            {link}
          </Text>
        </Box>
        <OpenLinkIcon aria-hidden height={24} />
      </BaseBox>
    </PemerintahDaerahLink>
  );
};

export default PemerintahDaerahCard;
