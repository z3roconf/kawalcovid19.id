import * as React from 'react';
import useSWR from 'swr/dist';

import styled from '@emotion/styled';
import * as Sentry from '@sentry/browser';
// import { NextComponentType, GetServerSideProps, NextPage } from 'next';
import { Box, Heading, themeProps, Text, Stack } from 'components/design-system';
import { fetch } from 'utils/api';
// import { logEventClick } from 'utils/analytics';
import formatTime from 'utils/formatTime';

import { PENELITIAN_VAKSIN_API_URL, PenelitianResponse, Penelitian } from 'types/vaksin';
import formatNumber from 'utils/formatNumber';

const EMPTY_DASH = '----';
const ERROR_TITLE = 'Error - Gagal mengambil data terbaru';
const ERROR_MESSAGE = 'Gagal mengambil data. Mohon coba lagi dalam beberapa saat.';

const GridWrapper = styled(Box)`
  display: grid;
  grid-template-rows: repeat(auto-fill, 1fr);
  grid-gap: ${themeProps.space.md}px;

  ${themeProps.mediaQueries.sm} {
    grid-template-columns: repeat(3, 1fr);
  }
`;

export interface ResearchBoxProps {
  color: string;
  value?: number;
  title: string;
  description: string;
}

const ResearchBox: React.FC<ResearchBoxProps> = React.memo(
  ({ color, value, title, description }) => (
    <Box
      display="flex"
      alignItems="flex-start"
      justifyContent="flex-start"
      px="xl"
      pt="xl"
      pb="xl"
      borderRadius={6}
      backgroundColor="card"
    >
      <Box textAlign="start" display="flex" flexDirection="column" flex={1}>
        <Text display="block" color="accents08" variant={600}>
          {title}
        </Text>
        <Text display="block" variant={1100} color={color} fontFamily="monospace">
          {formatNumber(value) || EMPTY_DASH}
        </Text>
        <Text display="block" variant={400} color="accents07">
          {description}
        </Text>
      </Box>
    </Box>
  )
);

export interface ResearchSectionBlockProps {
  data?: Penelitian;
  error?: boolean;
}

const ResearchSectionBlock: React.FC<ResearchSectionBlockProps> = React.memo(({ data, error }) => {
  if (error) {
    Sentry.withScope(scope => {
      scope.setTag('api_error', 'penelitian_vaksin');
      Sentry.captureException(error);
    });
  }
  const SectionHeading = styled(Heading)`
    margin-bottom: ${themeProps.space.md}px;

    ${themeProps.mediaQueries.md} {
      margin-bottom: ${themeProps.space.xl}px;
    }
  `;

  const DetailsWrapper = styled(Box)`
    ${themeProps.mediaQueries.md} {
      display: flex;
      align-items: flex-end;
      justify-content: space-between;
    }
  `;

  return (
    <Stack mb="xxl" mt={50}>
      <SectionHeading variant={800} as="h2">
        Perkembangan Penelitian Vaksin COVID-19 Dunia
      </SectionHeading>

      <Box>
        <GridWrapper>
          <ResearchBox
            color="brandred"
            title="Preclinical"
            description="Not yet in human trials"
            value={data?.preclinical}
          />
          <ResearchBox
            color="warning02"
            title="Phase I"
            description="Small-scale safety trials"
            value={data?.phase1}
          />
          <ResearchBox
            color="chart"
            title="Phase II"
            description="Expanded safety trials"
            value={data?.phase2}
          />
          <ResearchBox
            color="primary02"
            title="Phase III"
            description="Large-scale trials"
            value={data?.phase3}
          />
          <ResearchBox
            color="highlight03"
            title="Limited approval"
            description="Approved for limited use"
            value={data?.limitedApproval}
          />
          <ResearchBox
            color="success02"
            title="Approved"
            description="Approved for human use"
            value={data?.approved}
          />
        </GridWrapper>
        <DetailsWrapper>
          <Box mt="md">
            <Text as="h5" m={0} variant={200} color="accents04" fontWeight={400}>
              {error ? ERROR_TITLE : 'Pembaruan Terakhir'}
            </Text>
            <Text as="p" variant={400} color="accents07" fontFamily="monospace">
              {data?.lastUpdate ? formatTime(new Date(data?.lastUpdate), 'longest') : ERROR_MESSAGE}
            </Text>
          </Box>
        </DetailsWrapper>
      </Box>
    </Stack>
  );
});

const ResearchSection: React.FC = () => {
  const { data, error } = useSWR<PenelitianResponse | undefined>(PENELITIAN_VAKSIN_API_URL, fetch);

  return <ResearchSectionBlock data={data?.penelitianVaksin} error={error} />;
};

export default ResearchSection;
