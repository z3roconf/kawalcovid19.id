## Summary

Brief description of the issue.
Generally contains background/problems, goals/metrics, and/or expected outputs.

## User input

User suggestions, discussions, survey results, chat screenshots, or anything else that might help providing _user_ context.
_Delete this section if empty._

## Design

Mockups, figma links, animations, or anything else that guides to visual/user experience design implementations.
_Delete this section if empty._

## Tech/Implementation details

What to do, what should go where, how to do that, etc.

- [ ] May also be listed as to-do list
- [ ] Something like this
- [ ] (_mobile web specific implementations_) Mobile: insert something in somewhere
- [ ] (_desktop web specific implementations_) Desktop: change somenthing into something else

## Reference

Suggestions, discussions, slack thread links, chat screenshots, or anything else that might help providing context.
_Delete this section if empty._
