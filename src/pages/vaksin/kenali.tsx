import * as React from 'react';
import { NextPage } from 'next';
import styled from '@emotion/styled';
import { WordPressPost } from 'types/wp';
import { PageWrapper, Content, Column } from 'components/layout';
import { PostHeader } from 'modules/posts-index';
import { ResearchSection, CandidateTable } from 'modules/vaksin';

interface PostContentPageProps {
  post?: WordPressPost;
  errors?: string;
}

const ContentAsSection = Content.withComponent('section');

const Section = styled(ContentAsSection)`
  padding-bottom: 48px;
`;

const FAQPage: NextPage<PostContentPageProps> = () => {
  return (
    <PageWrapper title={`Kenali Vaksin | KawalCOVID19`} pageTitle="Kenali Vaksin">
      <PostHeader title="Informasi Seputar Vaksin COVID-19" />
      <Section>
        <Column>
          <ResearchSection />
          <CandidateTable />
        </Column>
      </Section>
    </PageWrapper>
  );
};

export default FAQPage;
